package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.model.Match;

import java.util.concurrent.ExecutionException;

public class EditMatchActivity extends AppCompatActivity {
    private Match match;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_match);
        TextView txt_titre = findViewById(R.id.txt_titre);
        TextView txt_equipe1 = findViewById(R.id.txt_equipe1);
        TextView txt_equipe2 = findViewById(R.id.txt_equipe2);
        TextView txtV_scoreE1 = findViewById((R.id.txtV_scoreE1));
        TextView txtV_scoreE2 = findViewById((R.id.txtV_scoreE2));

        Button btnscore = findViewById((R.id.btn_modif_score));
        Button btnsuppr = findViewById((R.id.btn_suppr));
        Button btn_back = findViewById((R.id.btn_back));
        Button btn_modif_titre = findViewById((R.id.btn_modif_titre));

        Button btn_modif_nomE1 = findViewById((R.id.btn_modif_nomE1));
        Button btn_modif_nomE2 = findViewById((R.id.btn_modif_nomE2));

        Button btnJoueur = findViewById((R.id.btn_pl));

        Bundle b = getIntent().getExtras();
        if (b!=null){
            int codeMatch = (int)b.getLong("codeMatch");
            try {
                match = new RoomDatabaseM2L.selectMatchsById(getApplicationContext()).execute(codeMatch).get();

                txt_titre.setText(match.getTitre());
                txt_equipe1.setText((match.getEquipe1()));
                txt_equipe2.setText((match.getEquipe2()));
                txtV_scoreE1.setText((match.getScoreE1()));
                txtV_scoreE2.setText((match.getScoreE2()));


                btnsuppr.setOnClickListener(observateurClickBouton);
                btnscore.setOnClickListener(observateurClickBouton);
                btnJoueur.setOnClickListener(observateurClickBouton);
                btn_back.setOnClickListener(observateurClickBouton);
                btn_modif_titre.setOnClickListener(observateurClickBouton);

                btn_modif_nomE1.setOnClickListener(observateurClickBouton);
                btn_modif_nomE2.setOnClickListener(observateurClickBouton);

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    private View.OnClickListener observateurClickBouton = new View.OnClickListener() {
        public void onClick(View v) {
            EditText txttitre = findViewById(R.id.txt_nouv_titre);
            EditText txtscoreE1 = findViewById(R.id.txt_nouv_score_equi1);
            EditText txtscoreE2 = findViewById(R.id.txt_nouv_score_equi2);

            EditText txtnomE1 = findViewById(R.id.txt_nouv_nom_equi1);
            EditText txtnomE2 = findViewById(R.id.txt_nouv_nom_equi2);

            Intent i ;
            switch (v.getId()) {
                case R.id.btn_modif_titre:
                    new RoomDatabaseM2L.updateTitreMatch(getApplicationContext()).execute(String.valueOf(match.getId()),txttitre.getText().toString());
                    setResult(Activity.RESULT_OK);
                    break;

                case R.id.btn_back:
                    i = new Intent(getApplicationContext(),MatchsActivity.class);
                    i.putExtra("codeEvt",match.geteventId());
                    startActivity(i);
                    setResult(Activity.RESULT_OK);
                    break;

                case R.id.btn_modif_score:
                    new RoomDatabaseM2L.updateMatch(getApplicationContext()).execute(String.valueOf(match.getId()),txtscoreE1.getText().toString(),txtscoreE2.getText().toString());
                    setResult(Activity.RESULT_OK);
                    break;

                case R.id.btn_modif_nomE1:
                    new RoomDatabaseM2L.updateEquipe1Match(getApplicationContext()).execute(String.valueOf(match.getId()),txtnomE1.getText().toString());
                    setResult(Activity.RESULT_OK);
                    break;
                case R.id.btn_modif_nomE2:
                    new RoomDatabaseM2L.updateEquipe2Match(getApplicationContext()).execute(String.valueOf(match.getId()),txtnomE2.getText().toString());
                    setResult(Activity.RESULT_OK);
                    break;

                case R.id.btn_suppr:
                    new RoomDatabaseM2L.delMatch(getApplicationContext()).execute(match.getId());
                    setResult(Activity.RESULT_OK);
                    finish();
                    break;

                case R.id.btn_pl:
                    i = new Intent(getApplicationContext(),EditPlayerActivity.class);
                    i.putExtra("equipe1",match.getEquipe1());
                    i.putExtra("equipe2",match.getEquipe2());
                    startActivity(i);
                    setResult(Activity.RESULT_OK);
                    break;


            }
        }
    };
}
