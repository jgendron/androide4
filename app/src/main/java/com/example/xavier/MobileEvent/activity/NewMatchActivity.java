package com.example.xavier.MobileEvent.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;

import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.model.Match;
import com.example.xavier.MobileEvent.R;

import java.util.Date;

/**
 * Created by x
 */
public class NewMatchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_match);
        Button btnAddMatch = findViewById((R.id.btnAddMatch));
        btnAddMatch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnAddMatch:
                        Bundle b = getIntent().getExtras();
                        if (b!=null){
                            int codeEvt = b.getInt("codeEvt");

                            EditText txtTitre = findViewById(R.id.txt_titreMatch);
                            String titre = txtTitre.getText().toString();

                            EditText txtE1 = findViewById(R.id.nomE1);
                            String equipe1 = txtE1.getText().toString();

                            EditText txtE2 = findViewById(R.id.nomE2);
                            String equipe2 = txtE2.getText().toString();

                            String scoreE1 = "0";
                            String scoreE2 = "0";

                            CalendarView calDate = findViewById(R.id.calDateMatch);
                            Date selectedDate = new Date(calDate.getDate());


                            Match match = new Match(titre,equipe1,equipe2,selectedDate,scoreE1,scoreE2,codeEvt);
                            new RoomDatabaseM2L.addMatch(getApplicationContext()).execute(match);
                        }
                        break;
                }
                finish();
            }
        });
    }
}
