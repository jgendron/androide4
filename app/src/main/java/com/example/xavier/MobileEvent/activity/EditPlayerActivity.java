package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.xavier.MobileEvent.R;

import com.example.xavier.MobileEvent.model.Joueur;

public class EditPlayerActivity extends AppCompatActivity {
    private Joueur player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_joueur);
        Button btnadd = findViewById((R.id.btn_addJ));
        Button btnvoirpl = findViewById((R.id.btn_voirPlayer));

        btnadd.setOnClickListener(observateurClickBouton);
        btnvoirpl.setOnClickListener(observateurClickBouton);}

        private View.OnClickListener observateurClickBouton = new View.OnClickListener() {
            public void onClick(View v) {
                Intent i;
                switch (v.getId()) {
                    case R.id.btn_addJ:
                        i = new Intent(getApplicationContext(), NewPlayerActivity.class);
                        Bundle b = getIntent().getExtras();
                        String equipe1 = (String) b.get("equipe1");
                        String equipe2 = (String) b.get("equipe2");

                        i.putExtra("equipe1", equipe1);
                        i.putExtra("equipe2", equipe2);

                        startActivity(i);
                        setResult(Activity.RESULT_OK);
                        break;
                    case R.id.btn_voirPlayer:
                        i = new Intent(getApplicationContext(), PlayerActivity.class);
                        startActivity(i);
                        setResult(Activity.RESULT_OK);
                        break;
                }
            }
        };
    }

