package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.xavier.MobileEvent.model.Match;

import java.util.List;

/**
 * Created by x
 */
@Dao
public interface MatchDAO {
    @Insert
    void insertMatch(Match match);

    @Query("SELECT * FROM `Match`")
    List<Match> getAllMatch();

    @Query("SELECT * FROM `Match` WHERE id=:id")
    Match getMatchById(int id);


    @Query("SELECT * FROM `Match` WHERE Eventid=:eventId")
    List<Match> getMatchByeventId(int eventId);


    @Query("UPDATE 'Match' SET scoreE1=:scoreE1, scoreE2=:scoreE2 WHERE id=:id")
    void updateMatch(int id,String scoreE1, String scoreE2);

    @Query("UPDATE 'Match' SET equipe1=:equipe1 WHERE id=:id")
    void updateEquipe1Match(int id,String equipe1);

    @Query("UPDATE 'Match' SET equipe2=:equipe2 WHERE id=:id")
    void updateEquipe2Match(int id,String equipe2);

    @Query("UPDATE 'Match' SET titre=:titre WHERE id=:id")
    void updateTitreMatch(int id,String titre);

    @Query("DELETE FROM 'Match' WHERE  id=:id")
    void deleteMatch(int id);




}
