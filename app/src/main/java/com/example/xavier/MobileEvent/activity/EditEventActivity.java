package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.model.Event;
import com.example.xavier.MobileEvent.R;

import java.util.concurrent.ExecutionException;

/**
 * Created by x
 */

public class EditEventActivity extends AppCompatActivity {
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        EditText txtEvt = findViewById(R.id.txt_event);
        Button btnAddMatch = findViewById((R.id.btnAddMatch));
        Button btnUpdate = findViewById((R.id.btnUpdate));
        Button btnDelete = findViewById((R.id.btnDelete));
        Button btnMatchs = findViewById((R.id.btnEventMatchs));
        Bundle b = getIntent().getExtras();
        if (b!=null){
            int codeEvt = (int)b.getLong("codeEvt");
            try {
                event = new RoomDatabaseM2L.selectEventById(getApplicationContext()).execute(codeEvt).get();
                txtEvt.setText(event.getTitre());
                btnDelete.setOnClickListener(observateurClickBouton);
                btnUpdate.setOnClickListener(observateurClickBouton);
                btnAddMatch.setOnClickListener(observateurClickBouton);
                btnMatchs.setOnClickListener(observateurClickBouton);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    private View.OnClickListener observateurClickBouton = new View.OnClickListener() {
        public void onClick(View v) {
            EditText txtEvt = findViewById(R.id.txt_event);
            Intent i ;
            switch (v.getId()) {
                case R.id.btnUpdate:
                    new RoomDatabaseM2L.updateEvent(getApplicationContext()).execute(String.valueOf(event.getId()),txtEvt.getText().toString());
                    setResult(Activity.RESULT_OK);
                    break;
                case R.id.btnDelete:
                    new RoomDatabaseM2L.delEvent(getApplicationContext()).execute(event.getId());
                    setResult(Activity.RESULT_OK);
                    finish();
                    break;
                case R.id.btnAddMatch:
                    i = new Intent(getApplicationContext(),NewMatchActivity.class);
                    i.putExtra("codeEvt",event.getId());
                    startActivity(i);
                    setResult(Activity.RESULT_OK);
                    break;
                case R.id.btnEventMatchs:
                    i = new Intent(getApplicationContext(),MatchsActivity.class);
                    i.putExtra("codeEvt",event.getId());
                    startActivity(i);
                    setResult(Activity.RESULT_OK);
                    break;
            }
        }
    };
}
