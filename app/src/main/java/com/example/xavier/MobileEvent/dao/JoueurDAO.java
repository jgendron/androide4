package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.xavier.MobileEvent.model.Joueur;

import java.util.List;

/**
 * Created by j
 */
@Dao
public interface JoueurDAO {
    @Insert
    void insertJoueur(Joueur joueur);

    @Query("SELECT * FROM Joueur")
    List<Joueur> getAllJoueur();

    @Query("SELECT * FROM Joueur WHERE matchId=:id ORDER BY equipe")
    List<Joueur> getJoueurs(int id);
/*
    @Query("SELECT * FROM Joueur WHERE id=:id")
    Joueur getJoueurById(int id);

    @Query("DELETE FROM Joueur WHERE  id=:id")
    void deleteJoueur(int id);

    @Query("UPDATE Joueur SET nom=:nom WHERE id=:id")
    void updatenomJoueur(int id, String nom);

    @Query("UPDATE Joueur SET prenom=:prenom WHERE id=:id")
    void updateprenomJoueur(int id, String prenom);
*/
}
