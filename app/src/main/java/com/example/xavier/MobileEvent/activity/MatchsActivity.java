package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;

import com.example.xavier.MobileEvent.model.Match;
import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.listAdapter.MatchListAdapter;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MatchsActivity extends AppCompatActivity {

    private static final int CODE_RETOUR_NEW_MATCH = 1;
    private static final int CODE_RETOUR_EDIT_MATCH = 2;

    private List<Match> allMatch = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);
        populateListView();
        Bundle b = getIntent().getExtras();
        if (b!=null) {
            int codeMatch = b.getInt("codeEvt");
            try {
                List<Match> matchs = new RoomDatabaseM2L.getMatchByeventId(getApplicationContext()).execute(codeMatch).get();
                MatchListAdapter adapter = new MatchListAdapter(getApplicationContext(), matchs);
                ListView list = findViewById(R.id.lst_match);
                list.setAdapter(adapter);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    private void populateListView(){
        try {
            allMatch = new RoomDatabaseM2L.selectMatchs(getApplicationContext()).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        MatchListAdapter adapter = new MatchListAdapter(getApplicationContext(), allMatch);
        ListView list = findViewById(R.id.lst_match);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Intent intent = new Intent(getApplicationContext(),EditMatchActivity.class);
                intent.putExtra("codeMatch",itemId);
                startActivityForResult(intent, CODE_RETOUR_EDIT_MATCH);
            }
        });
    }
}
