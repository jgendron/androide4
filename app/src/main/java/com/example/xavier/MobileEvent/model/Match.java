package com.example.xavier.MobileEvent.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by x
 */

@Entity
public class Match {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private Date dateRencontre;
    @NonNull
    private String titre;

    @NonNull
    private String equipe1;
    @NonNull
    private String equipe2;

    private String scoreE1;
    private String scoreE2;

    @ForeignKey(entity = Event.class, parentColumns = "id", childColumns = "eventId")
    private int eventId;

    public int getEventId() {
        return eventId;
    }

    public Match(@NonNull String titre,@NonNull String equipe1,@NonNull String equipe2, @NonNull Date dateRencontre, @NonNull String scoreE1,@NonNull String scoreE2, int eventId) {
        this.titre=titre;
        this.equipe1=equipe1;
        this.equipe2=equipe2;
        this.dateRencontre=dateRencontre;
        this.eventId = eventId;
        this.scoreE1 = scoreE1;
        this.scoreE2 = scoreE2;
    }

    @NonNull
    public Date getDateRencontre() {
        return dateRencontre;
    }

    @NonNull
    public String getTitre() {
        return titre;
    }

    public void setTitre(@NonNull String titre) {
        this.titre = titre;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int geteventId() {
        return this.eventId;
    }

    public void seteventId(int eventId) {
        this.eventId = eventId;
    }

    public String getScoreE1() {
        return scoreE1;
    }

    public void setScoreE1(String scoreE1) {
        this.scoreE1 = scoreE1;
    }

    public String getScoreE2() {
        return scoreE2;
    }
    public void setScoreE2(String scoreE2) {
        this.scoreE2 = scoreE2;
    }

    @NonNull
    public String getEquipe1() {
        return equipe1;
    }
    public void setEquipe1(String equipe1) {
        this.equipe1 = equipe1;
    }

    @NonNull
    public String getEquipe2() {
        return equipe2;
    }
    public void setEquipe2(String equipe2) {
        this.equipe2 = equipe2;
    }
}
