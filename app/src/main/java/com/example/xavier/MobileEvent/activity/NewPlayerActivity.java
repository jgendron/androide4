package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.listAdapter.MatchListAdapter;
import com.example.xavier.MobileEvent.model.Joueur;
import com.example.xavier.MobileEvent.model.Match;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class NewPlayerActivity extends AppCompatActivity {
    Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);

        Button btnSave = findViewById((R.id.btnPlayer_Save));
        Button btnback = findViewById((R.id.Player_Back));
        spinner = findViewById(R.id.spinnerTeam);

        btnSave.setOnClickListener(observateurClickBouton);
        btnback.setOnClickListener(observateurClickBouton);
        TextView choixequipe = findViewById((R.id.choixequipe));


        Bundle b = getIntent().getExtras();

        String equipe1 = (String) b.get("equipe1");
        String equipe2 = (String) b.get("equipe2");

        List testequipe = new ArrayList();
        testequipe.add(equipe1);
        testequipe.add(equipe2);

        ArrayAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                testequipe
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        }



    private View.OnClickListener observateurClickBouton = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i;
            switch (v.getId()) {
                case R.id.Player_Back:
                    i = new Intent(getApplicationContext(),EditPlayerActivity.class);
                    startActivity(i);
                    setResult(Activity.RESULT_OK);
                    break;

                case R.id.btnSave:
                    Bundle b = getIntent().getExtras();
                    if (b!=null){
                        int codeMatch = b.getInt("codeMatch");

                        EditText txtNom = findViewById(R.id.txt_newPlayerName);
                        String Nom = txtNom.getText().toString();

                        EditText txtPrenom = findViewById(R.id.txt_newPlayerName2);
                        String Prenom = txtPrenom.getText().toString();

                        Joueur joueur = new Joueur(Prenom,Nom,spinner.getSelectedItem().toString(),codeMatch);
                        new RoomDatabaseM2L.addJoueur(getApplicationContext()).execute(joueur);
                    }
                    break;
            }
            finish();
        }
    };

}
