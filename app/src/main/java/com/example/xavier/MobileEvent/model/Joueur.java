package com.example.xavier.MobileEvent.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;



@Entity
public class Joueur {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String nom;
    @NonNull
    private String prenom;
    @NonNull
    private String equipe;

    @ForeignKey(entity = Match.class, parentColumns = "id", childColumns = "matchId")
    private int matchId;

    public int getMatchId() {
        return matchId;
    }

    public Joueur(@NonNull String prenom, @NonNull String nom, @NonNull String equipe, int matchId) {
        this.prenom=prenom;
        this.nom=nom;
        this.matchId = matchId;
        this.equipe = equipe;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    public void setnom(@NonNull String nom) {
        this.nom = nom;
    }
    public void setprenom(@NonNull String prenom) {
        this.prenom = prenom;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(@NonNull String equipe) {
        this.equipe = equipe;
    }
}


