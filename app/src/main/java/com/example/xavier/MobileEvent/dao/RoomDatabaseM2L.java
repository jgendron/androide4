package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;


import com.example.xavier.MobileEvent.util.DateConverters;
import com.example.xavier.MobileEvent.model.Event;
import com.example.xavier.MobileEvent.model.Match;
import com.example.xavier.MobileEvent.model.Joueur;

import java.util.List;
/**
 * Created by x
 */
@Database(entities = {Event.class,Match.class,Joueur.class},version = 1,exportSchema = false)
@TypeConverters(DateConverters.class)
public abstract class RoomDatabaseM2L extends RoomDatabase {
    public abstract EventDAO eventDAO();

    public abstract MatchDAO matchDAO();

    public abstract JoueurDAO joueurDAO();

    private static volatile RoomDatabaseM2L INSTANCE;

    static RoomDatabaseM2L getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabaseM2L.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RoomDatabaseM2L.class, "M2LDatabase").build();
                }
            }
        }
        return INSTANCE;
    }

    // ADD

    public static class addEvent extends AsyncTask<Event, Void, Void> {
        private EventDAO eventDao;

        public addEvent(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao = db.eventDAO();
        }

        @Override
        protected Void doInBackground(Event... events) {
            eventDao.insertEvent(events[0]);
            return null;
        }
    }

    public static class addMatch extends AsyncTask<Match, Void, Void> {
        private MatchDAO matchDao;

        public addMatch(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDao = db.matchDAO();
        }

        @Override
        protected Void doInBackground(Match... matches) {
            matchDao.insertMatch(matches[0]);
            return null;
        }
    }

    public static class addJoueur extends AsyncTask<Joueur, Void, Void> {
        private JoueurDAO joueurDao;

        public addJoueur(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            joueurDao = db.joueurDAO();
        }

        @Override
        protected Void doInBackground(Joueur... joueurs) {
            joueurDao.insertJoueur(joueurs[0]);
            return null;
        }
    }

    //SELECT

    public static class selectEvents extends AsyncTask<Void, Void, List<Event>> {
        private EventDAO eventDao;

        public selectEvents(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao = db.eventDAO();
        }

        @Override
        protected List<Event> doInBackground(Void... voids) {
            List<Event> allEvents;
            allEvents = eventDao.getAllEvents();
            return allEvents;
        }
    }

    public static class selectMatchs extends AsyncTask<Integer, Void, List<Match>> {
        private MatchDAO matchDao;

        public selectMatchs(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDao = db.matchDAO();
        }

        @Override
        protected List<Match> doInBackground(Integer... integers) {
            List<Match> allMatchs;
            allMatchs = matchDao.getAllMatch();
            return allMatchs;
        }
    }


    public static class selectJoueurs extends AsyncTask<Integer, Void, List<Joueur>> {
        private JoueurDAO joueurDao;

        public selectJoueurs(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            joueurDao = db.joueurDAO();
        }

        @Override
        protected List<Joueur> doInBackground(Integer... integers) {
            List<Joueur> allJoueurs;
            allJoueurs = joueurDao.getAllJoueur();
            return allJoueurs;
        }
    }

    //SELECT BY

    public static class selectEventById extends AsyncTask<Integer, Void, Event> {
        private EventDAO eventDao;

        public selectEventById(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao = db.eventDAO();
        }

        @Override
        protected Event doInBackground(Integer... integers) {
            Event event;
            event = eventDao.getEventById(integers[0]);
            return event;
        }
    }


    public static class selectMatchsById extends AsyncTask<Integer, Void, Match> {
        private MatchDAO matchDAO;

        public selectMatchsById(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Match doInBackground(Integer... integers) {
            Match match;
            match = matchDAO.getMatchById(integers[0]);
            return match;
        }
    }

    public static class getMatchByeventId extends AsyncTask<Integer, Void, List<Match>> {
        private MatchDAO matchDAO;

        public getMatchByeventId(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected List<Match> doInBackground(Integer... integers) {
            List<Match> matchEvt;
            matchEvt = matchDAO.getMatchByeventId(integers[0]);
            return matchEvt;
        }
    }


    //DELETE

    public static class delEvent extends AsyncTask<Integer, Void, Void> {
        private EventDAO eventDao;

        public delEvent(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao = db.eventDAO();
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            eventDao.deleteEvent(integers[0]);
            return null;
        }
    }

    public static class delMatch extends AsyncTask<Integer, Void, Void> {
        private MatchDAO matchDAO;

        public delMatch(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            matchDAO.deleteMatch(integers[0]);
            return null;
        }
    }

    //UPDATE

    public static class updateEvent extends AsyncTask<String, Void, Void> {
        private EventDAO eventDao;

        public updateEvent(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao = db.eventDAO();
        }

        @Override
        protected Void doInBackground(String... strings) {
            eventDao.updateEvent(Integer.parseInt(strings[0]), strings[1]);
            return null;
        }
    }

    public static class updateMatch extends AsyncTask<String, Void, Void> {
        private MatchDAO matchDAO;

        public updateMatch(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Void doInBackground(String... strings) {
            matchDAO.updateMatch(Integer.parseInt(strings[0]), strings[1], strings[2]);
            return null;
        }
    }

    public static class updateEquipe1Match extends AsyncTask<String, Void, Void> {
        private MatchDAO matchDAO;

        public updateEquipe1Match(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Void doInBackground(String... strings) {
            matchDAO.updateEquipe1Match(Integer.parseInt(strings[0]), strings[1]);
            return null;
        }
    }

    public static class updateEquipe2Match extends AsyncTask<String, Void, Void> {
        private MatchDAO matchDAO;

        public updateEquipe2Match(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Void doInBackground(String... strings) {
            matchDAO.updateEquipe2Match(Integer.parseInt(strings[0]), strings[1]);
            return null;
        }
    }

    public static class updateTitreMatch extends AsyncTask<String, Void, Void> {
        private MatchDAO matchDAO;

        public updateTitreMatch(Context context) {
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDAO = db.matchDAO();
        }

        @Override
        protected Void doInBackground(String... strings) {
            matchDAO.updateTitreMatch(Integer.parseInt(strings[0]), strings[1]);
            return null;
        }

    }
}
