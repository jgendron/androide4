package com.example.xavier.MobileEvent.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.xavier.MobileEvent.model.Match;
import com.example.xavier.MobileEvent.R;

import java.util.List;

/**
 * Created by x
 */

public class MatchListAdapter extends BaseAdapter {
    private List<Match> matchList;
    private LayoutInflater inflater;

    public MatchListAdapter(Context context, List<Match> listM){
        this.inflater=LayoutInflater.from(context);
        this.matchList =listM;
    }
    @Override
    public int getCount() {
        return this.matchList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.matchList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.matchList.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtMatch;
        if(convertView==null){
            convertView = this.inflater.inflate(R.layout.item_match,parent,false);
            txtMatch = convertView.findViewById(R.id.txt_match);
            convertView.setTag(R.id.txt_match,txtMatch);
        }
        else{
            txtMatch = (TextView) convertView.getTag(R.id.txt_evt);
        }
        Match match = (Match) this.getItem(position);
        txtMatch.setText(match.getTitre()+"   "+match.getEquipe1()+" / "+match.getEquipe2()+"    Le score est de "+ match.getScoreE1()+" - "+ match.getScoreE2());
        return convertView;
    }

}
