package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.xavier.MobileEvent.model.Event;

import java.util.List;

/**
 * Created by x
 */
@Dao
public interface    EventDAO {
    @Insert
    void insertEvent (Event event);

    @Query("SELECT * FROM Event")
    List<Event> getAllEvents();

    @Query("SELECT * FROM Event WHERE id=:id")
    Event getEventById(int id);

    @Query("DELETE FROM Event WHERE  id=:id")
    void deleteEvent(int id);

    @Query("UPDATE Event SET titre=:titre WHERE id=:id")
    void updateEvent(int id,String titre);
}
