package com.example.xavier.MobileEvent.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.listAdapter.PlayerListAdapter;
import com.example.xavier.MobileEvent.model.Joueur;


import java.util.List;
import java.util.concurrent.ExecutionException;

public class PlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            int codeJoueur = b.getInt("codeJoueur");
            try {
                List<Joueur> joueurs = new RoomDatabaseM2L.selectJoueurs(getApplicationContext()).execute(codeJoueur).get();
                PlayerListAdapter adapter = new PlayerListAdapter(getApplicationContext(), joueurs);
                ListView list = findViewById(R.id.lst_player);
                list.setAdapter(adapter);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }
}
