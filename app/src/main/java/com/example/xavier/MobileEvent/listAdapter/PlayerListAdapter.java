package com.example.xavier.MobileEvent.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.model.Joueur;


import java.util.List;

public class PlayerListAdapter extends BaseAdapter {
    private List<Joueur> joueurList;
    private LayoutInflater inflater;

    public PlayerListAdapter(Context context, List<Joueur> listJ){
        this.inflater=LayoutInflater.from(context);
        this.joueurList =listJ;
    }
    @Override
    public int getCount() {
        return this.joueurList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.joueurList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.joueurList.get(i).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtPlayer;
        if(convertView==null){
            convertView = this.inflater.inflate(R.layout.item_match,parent,false);
            txtPlayer = convertView.findViewById(R.id.txt_match);
            convertView.setTag(R.id.txt_match,txtPlayer);
        }
        else{
            txtPlayer = (TextView) convertView.getTag(R.id.txt_evt);
        }
        Joueur joueur = (Joueur) this.getItem(position);
        txtPlayer.setText(joueur.getNom());
        return convertView;
    }

}
